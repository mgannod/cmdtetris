Compiler: https://jmeubank.github.io/tdm-gcc/

This is a tetris implemented for the command line. This is implemented for windows only, I don't know how it will behave with other platforms.

To Navigate the menu use "w" and "s" for up and down and press "enter" for your selection.

Controls:
"a", "d" for left, right respectively.
"j", "l" for left rotation and right rotation respectively.

Hold "s" for soft drop, "w" for hard drop

Press "p" key to stop.

TODO:
1. Create main menu.
2. Save best scores to a file and display them.