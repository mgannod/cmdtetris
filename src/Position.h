#ifndef POSITION_H
#define POSITION_H

using namespace std;

class Position
{
    private:
    int x, y;

    public:
    Position(int x = 0, int y = 0);
    void SetPosition(int x, int y);
    int GetX();
    int GetY();
};
#endif