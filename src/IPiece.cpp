#include "Piece.h"

using namespace std;

IPiece::IPiece(Position* pos) : Piece(pos, 4)
{
    up = false;
    SetCells();
}

//Returns the initial position of a TPiece from the pivot point
void IPiece::SetCells()
{
    cells[1] = new Position(pivot->GetX() + 1, pivot->GetY());
    cells[2] = new Position(pivot->GetX() - 1, pivot->GetY());
    cells[3] = new Position(pivot->GetX() - 2, pivot->GetY());
}

void IPiece::LRotation()
{
    if(up)
        Piece::LRotation();
    else
        Piece::RRotation();

    up = !up;
}

void IPiece::RRotation()
{
    IPiece::LRotation();
}