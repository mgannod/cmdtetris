#include "Score.h"

using namespace std;

Score::Score()
{
    score = 0;
}

void Score::LinesCleared(int level, int numLines)
{
    int flat = 0;

    switch(numLines)
    {
        case 1:
            flat = 40;
            break;
        case 2:
            flat = 100;
            break;
        case 3:
            flat = 300;
            break;
        case 4:
            flat = 1200;
            break;
    }
    
    score += flat * (level + 1);
}

int Score::GetScore()
{
    return score;
}