#include "Piece.h"

using namespace std;

OPiece::OPiece(Position* pos) : Piece(pos, 4)
{
    SetCells();
}

//Returns the initial position of a TPiece from the pivot point
void OPiece::SetCells()
{
    cells[1] = new Position(pivot->GetX() + 1, pivot->GetY());
    cells[2] = new Position(pivot->GetX() + 1, pivot->GetY() + 1);
    cells[3] = new Position(pivot->GetX(), pivot->GetY() + 1);
}

void OPiece::LRotation(){}
void OPiece::RRotation(){}