#include "Board.h"
#include "PieceFactory.h"

//CONSTRUCTOR
Board::Board()
{
    //fill the board with board cells
    for(int j = 0; j < HEIGHT; j++)
    {
        for(int i = 0; i < WIDTH; i++)
        {
            board[i][j] = BoardCell(Position(i, j));
        }
    }

    //setup das
    DAScounter = 0;
    initialDelay = true;
    rotationDAS = 0;
    initialRotation = true;
    fallCounter = 0;
    initialHardDrop = true;

    gameOver = false;

    lines = 0;
    startLevel = 0;
    linesToTransition = min(startLevel*10+10, max(100, startLevel*10-50));

    textPos = Position(25, 5);
    score = Score();
    pieceFactory = new PieceFactory();
    //set pieces
    nextPiece = pieceFactory->GetNextPiece();
    NextPiece();

    //Set Level
    SetLevel(0);
}

//Constructor to start at a specific level
Board::Board(int startLevel):Board()
{
    this->startLevel = startLevel;
    SetLevel(startLevel);
    linesToTransition = min(startLevel*10+10, max(100, startLevel*10-50));
}

Board::~Board()
{
    delete currentPiece;
    delete nextPiece;
    delete pieceFactory;
}

void Board::NextPiece()
{
    currentPiece = nextPiece;
    nextPiece = pieceFactory->GetNextPiece();
    PrintNextPiece();

    Position** pos = currentPiece->GetPositions();
    for(int i = 0; i < currentPiece->NUM_CELLS; i++)
    {
        if(board[pos[i]->GetX()][pos[i]->GetY()].GetValue() == 2)
            gameOver = true;
    }
    SetPieceValues(1);
}

void Board::SetLevel(int level)
{
    this->level = level;
    fallSpeed = GetFallSpeed(level);
    mvprintw(textPos.GetY(), textPos.GetX(), "Level %d", level, lines);
}

bool Board::Transition()
{
    bool transition = false;

    if(lines >= linesToTransition)
    {
        transition = true;
    }

    return transition;
}

bool Board::GameOver()
{
    return gameOver;
}

void Board::Update()
{
    //Q is pressed, rotate left
    if(GetKeyState('J') & 0x8000/*Check if high-order bit is set (1 << 15)*/&& !(GetKeyState('E') & 0x8000))
    {
        if(CheckRotationDAS())
            LRotation();
    }
    if(GetKeyState('L') & 0x8000/*Check if high-order bit is set (1 << 15)*/&& !(GetKeyState('Q') & 0x8000))
    {
        if(CheckRotationDAS())
            RRotation();
    }

    if(!(GetKeyState('J') & 0x8000) && !(GetKeyState('L') & 0x8000))
    {
        ResetRotationDAS();
    }
    
    //A is pressed, move left
    if(GetKeyState('A') & 0x8000/*Check if high-order bit is set (1 << 15)*/ && !(GetKeyState('D') & 0x8000))
    {
        if(CheckDAS())
            MoveLeft();
    }//Move right if d is pressed
    if(GetKeyState('D') & 0x8000/*Check if high-order bit is set (1 << 15)*/ && !(GetKeyState('A') & 0x8000))
    {
        if(CheckDAS())
            MoveRight();
    }//if input is not present for a frame, reset das
    
    if(!(GetKeyState('A') & 0x8000) && !(GetKeyState('D') & 0x8000))
    {
        ResetDAS();
    }

    //fall logic
    if(GetKeyState('S') & 0x8000/*Check if high-order bit is set (1 << 15)*/)
    {
        Drop(true);
    }
    else
    {
        Drop(false);
    }

    if(GetKeyState('W') & 0x8000/*Check if high-order bit is set (1 << 15)*/ && initialHardDrop)
    {
        HardDrop();
        initialHardDrop = false;
    }
    if(!(GetKeyState('W') & 0x8000))
    {
        initialHardDrop = true;
    }

    ClearLines();//check for full lines and clear them

    if(Transition())
    {
        SetLevel(level+1);
        linesToTransition += 10;
    }
}

void Board::SetPieceValues(int value)
{
    Position** pos = currentPiece->GetPositions();
    string output;

    for(int i = 0; i < currentPiece->NUM_CELLS; i++)
    {
        board[pos[i]->GetX()][pos[i]->GetY()].SetValue(value);
        //output block basted on value
        int val = board[pos[i]->GetX()][pos[i]->GetY()].GetValue();
        if(val == 1 || val == 2)
            output = "[]";
        else
            output = " .";

        int x = 1 + 2 * pos[i]->GetX();
        int y = 20 - pos[i]->GetY();
        if(y > 0)
            mvprintw(y, x, output.c_str());
    } 
}

void Board::MoveLeft()
{
    //Get position
    Position** pos = currentPiece->GetPositions();

    //Check bounds
    bool canMove = true;
    for(int i = 0; i < currentPiece->NUM_CELLS && canMove; i++)
    {
        //check out of bounds or into wall
        int x = pos[i]->GetX()-1;
        if(x < 0 || board[x][pos[i]->GetY()].GetValue() == 2)
            canMove = false;
    }

    //perform movement if left is a valid move
    if(canMove)
    {
        SetPieceValues(0);
        currentPiece->MoveLeft();
        SetPieceValues(1);
    }
}

void Board::MoveRight()
{
    //Get position
    Position** pos = currentPiece->GetPositions();

    //Check bounds
    bool canMove = true;
    for(int i = 0; i < currentPiece->NUM_CELLS && canMove; i++)
    {
        //check out of bounds or into wall
        int x = pos[i]->GetX()+1;
        if(x >= WIDTH || board[x][pos[i]->GetY()].GetValue() == 2)
            canMove = false;
    }

    //perform movement if left is a valid move
    if(canMove)
    {
        SetPieceValues(0);
        currentPiece->MoveRight();
        SetPieceValues(1);
    }
}

void Board::RRotation()
{
    //Get position
    Position** pos = currentPiece->GetPositions();
    
    SetPieceValues(0);
    currentPiece->RRotation();//perform rotation
    //Check bounds
    bool canRotate = true;
    for(int i = 0; i < currentPiece->NUM_CELLS && canRotate; i++)
    {
        //check out of bounds or into wall
        if(pos[i]->GetX() < 0 || pos[i]->GetX() >= WIDTH || pos[i]->GetY() < 0 || board[pos[i]->GetX()][pos[i]->GetY()].GetValue() == 2)
            canRotate = false;
    }
    currentPiece->LRotation();//reset rotation

    //perform rotation if valid rotation
    if(canRotate)
        currentPiece->RRotation();
    
    SetPieceValues(1);
}

void Board::LRotation()
{
    //Get position
    Position** pos = currentPiece->GetPositions();
    
    SetPieceValues(0);
    currentPiece->LRotation();//perform rotation
    //Check bounds
    bool canRotate = true;
    for(int i = 0; i < currentPiece->NUM_CELLS && canRotate; i++)
    {
        //check out of bounds or into wall
        if(pos[i]->GetX() < 0 || pos[i]->GetX() >= WIDTH || board[pos[i]->GetX()][pos[i]->GetY()].GetValue() == 2)
            canRotate = false;
    }
    currentPiece->RRotation();//reset rotation

    //perform rotation if valid rotation
    if(canRotate)
        currentPiece->LRotation();
        
    SetPieceValues(1);
}

void Board::Drop(bool softDrop)
{
    int time = fallSpeed;

    if(softDrop)
    {
        time  = (int)(time / 2);
    }

    if(fallCounter >= time)
    {
        if(PieceCanFall())
        {
            //perform fall
            SetPieceValues(0);
            currentPiece->Fall();
            SetPieceValues(1);
        }
        else
        {
            SetPieceValues(2);
            NextPiece();
        }
        fallCounter=0;
    }

    fallCounter++;
}

void Board::HardDrop()
{
    while(PieceCanFall())
    {
        //perform fall
        SetPieceValues(0);
        currentPiece->Fall();
        SetPieceValues(1);
    }

    //piece cannot fall
    SetPieceValues(2);
    NextPiece();
}

bool Board::PieceCanFall()
{
    Position** pos = currentPiece->GetPositions();

    bool canFall = true;
    for(int i = 0; i < currentPiece->NUM_CELLS; i++)
    {
        int y = pos[i]->GetY()-1;
        if(y < 0 || board[pos[i]->GetX()][y].GetValue() == 2)
            canFall = false;
    }

    return canFall;
}

//checks for lines and clears any that are full
void Board::ClearLines()
{
    bool lineEmpty = false;
    int numLines = 0;

    for(int row = 0; row < HEIGHT && !lineEmpty;)
    {
        int fullness = 0;//tracks number of blocks filled
        for(int col = 0; col < WIDTH; col++)
        {
            if(board[col][row].GetValue() == 2)
            {
                fullness++;//placed block found, increment
            }
        }
        if(fullness == 10)//row is full
        {
            ClearLine(row);//clear line shift above down
            numLines++;
        }
        else if(fullness == 0)//row is empty
        {
            lineEmpty = true;
        }
        else//row is not empty but not full, look at line above
        {
            row++;
        }
    }
    
    if(numLines > 0)
    {
        score.LinesCleared(level, numLines);
        mvprintw(textPos.GetY()+1, textPos.GetX(), "Lines Cleared: %d", lines);
    }

    mvprintw(textPos.GetY()+2, textPos.GetX(), "Score: %d", score.GetScore());
}

void Board::ClearLine(int row)
{
    lines++;
    //clear a line
    for(int i = 0; i < WIDTH; i++)
    {
        board[i][row].SetValue(0);
        mvprintw(20 - row, 1 + 2 * i, " .");
    }

    //move everything above down
    int numEmpty = 0;
    for(row = row+1; row < HEIGHT && numEmpty < 10; row++)
    {
        numEmpty = 0;
        for(int col = 0; col < WIDTH; col++)
        {
            BoardCell* cell = &board[col][row];
            if(cell->GetValue() == 2)
            {
                int y = 20 - row;
                int x = 1 + 2 * col;

                cell->SetValue(0);
                mvprintw(y, x, " .");

                board[col][row-1].SetValue(2);

                mvprintw(y + 1, x, "[]");
            }
            else
            {
                numEmpty++;
            }
        }
    }
}
//called on input to check whether can move
bool Board::CheckDAS()
{
    bool canMove = false;
    if(initialDelay)
    {
        if(DAScounter == 0)
        {
            canMove = true;
        }
        else if(DAScounter >= 16)
        {
            initialDelay = false;
            canMove = true;
            DAScounter = 0;
        }
    }
    else
    {
        if(DAScounter >= 6)
        {
            canMove = true;
            DAScounter = 0;
        }
    }

    DAScounter++;//increment das
    
    return canMove;
}

bool Board::CheckRotationDAS()
{
    bool canMove = false;
    if(initialRotation)
    {
        if(rotationDAS == 0)
        {
            canMove = true;
        }
        else if(rotationDAS >= 16)
        {
            initialRotation = false;
            canMove = true;
            rotationDAS = 0;
        }
    }
    else
    {
        if(rotationDAS >= 6)
        {
            canMove = true;
            rotationDAS = 0;
        }
    }

    rotationDAS++;//increment das
    
    return canMove;
}

void Board::ResetDAS()
{
    DAScounter = 0;
    initialDelay = true;
}

void Board::ResetRotationDAS()
{
    rotationDAS = 0;
    initialRotation = true;
}

int Board::GetFallSpeed(int level)
{
    int fallSpeed = 48;
    if(level < 9)
    {
        fallSpeed = fallSpeed - 5 * level;
    }
    else if(level < 29)
    {
        fallSpeed = 6;
        if(level >= 10)
            fallSpeed--;
        if(level >= 12)
            fallSpeed--;
        if(level >= 15)
            fallSpeed--;
        if(level >= 18)
            fallSpeed--;
        if(level >= 28)
            fallSpeed--;
    }

    return fallSpeed;
}

void Board::PrintNextPiece()
{
    Position** pos = nextPiece->GetPositions();
    Position previewBox = Position(textPos.GetX() + 4, textPos.GetY() + 6);

    //clear box
    mvprintw(previewBox.GetY() - 2, previewBox.GetX() - 4, "Next Piece:");
    for(int i = 0; i < 2; i++)
    {
        mvprintw(previewBox.GetY() + i, previewBox.GetX() - 4, "        ");
    }

    //print next piece
    for(int i = 0; i < nextPiece->NUM_CELLS; i++)
    {
        int x = previewBox.GetX() + 2 * (pos[i]->GetX() - pos[0]->GetX());
        int y = 1 + previewBox.GetY() - (pos[i]->GetY() - pos[0]->GetY());

        mvprintw(y, x, "[]");
    }
}

//Prints the string and returns the output string
string Board::PrintBoard()
{
    //output string
    string output = "";
    for(int j = HEIGHT-1; j >= 0; j--)
    {
        output += "*";//print boarder
        for(int i = 0; i < WIDTH; i++)
        {
            if(board[i][j].GetValue() == 1 || board[i][j].GetValue() == 2)
                output += "[]";
            else
                output += " .";
        }
        output += "*\n";//print boarder
    }
    //print bottom boarder
    for(int i = 0; i <= WIDTH; i++)
    {
        output += "**";
    }
    //print output
    printw(output.c_str());
    mvprintw(textPos.GetY(), textPos.GetX(), "Level %d", level);
    mvprintw(textPos.GetY()+1, textPos.GetX(), "Lines Cleared: %d", lines);
    mvprintw(textPos.GetY()+2, textPos.GetX(), "Score: %d", score.GetScore());
    PrintNextPiece();
    //return the string
    return output;
}

void Board::PrintScore()
{
    cout << "Lines Cleared: " << lines << endl;
    cout << "Score: " << score.GetScore() << endl;
}

int Board::GetScore()
{
    return score.GetScore();
}