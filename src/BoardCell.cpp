#include "BoardCell.h"
#include <iostream>
//CONSTRUCTOR
BoardCell::BoardCell(Position pos)
{
    cellValue = 0;
    this->pos = Position(pos.GetX(), pos.GetY());
}

Position BoardCell::GetPosition()
{
    return pos;
}

int BoardCell::GetValue()
{
    return cellValue;
}

void BoardCell::SetValue(int val)
{
    cellValue = val;
}