#include "Piece.h"

using namespace std;

LJPiece::LJPiece(bool isL, Position* pos) : Piece(pos, 4)
{
    this->isL = isL;
    SetCells();
}

//Returns the initial position of a TPiece from the pivot point
void LJPiece::SetCells()
{
    cells[1] = new Position(pivot->GetX() + 1, pivot->GetY());
    cells[2] = new Position(pivot->GetX() - 1, pivot->GetY());

    if(isL)
        cells[3] = new Position(pivot->GetX() + 1, pivot->GetY() + 1);
    else
        cells[3] = new Position(pivot->GetX() - 1, pivot->GetY() + 1);
}