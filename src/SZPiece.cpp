#include "Piece.h"

using namespace std;

SZPiece::SZPiece(bool isS, Position* pos) : Piece(pos, 4)
{
    this->isS = isS;
    SetCells();
}

//Returns the initial position of a TPiece from the pivot point
void SZPiece::SetCells()
{
    cells[1] = new Position(pivot->GetX(), pivot->GetY() + 1);

    if(isS)
    {
        cells[2] = new Position(pivot->GetX() + 1, pivot->GetY());
        cells[3] = new Position(pivot->GetX() - 1, pivot->GetY() + 1);
    }
    else
    {
        cells[2] = new Position(pivot->GetX() - 1, pivot->GetY());
        cells[3] = new Position(pivot->GetX() + 1, pivot->GetY() + 1);
    }
}