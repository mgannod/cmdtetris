#ifndef BOARD_H
#define BOARD_H

#include<iostream>
#include<string>
#include "..\libs\PDCurses-master\curses.h"
#include"BoardCell.h"
#include"Piece.h"
#include"Score.h"
class PieceFactory;

using namespace std;

class Board
{
    public:
    const static int WIDTH = 10;
    const static int HEIGHT = 20;
    string PrintBoard();
    void PrintScore();
    //CONSTRUCTOR
    Board();
    Board(int);//Start at specific level
    ~Board();

    void NextPiece();
    void Update();

    void MoveLeft();//moves piece left
    void MoveRight();//moves piece right

    void RRotation();//right rotation
    void LRotation();//left rotation

    void Drop(bool);//moves piece down. true if soft drop
    void HardDrop();

    void SetLevel(int);

    bool GameOver();
    int GetScore();

    private:
    int startLevel;//starting level
    int level;//current level
    int lines;//lines cleared
    int linesToTransition;//lines until next level
    int fallSpeed;//frames that it takes to fall one block

    bool gameOver;

    //DAS-delayed auto shift
    int DAScounter;
    bool initialDelay;
    int rotationDAS;
    bool initialRotation;
    int fallCounter;
    bool initialHardDrop;

    BoardCell board [WIDTH][HEIGHT+2];//2d array of size widthxheight, height is puls to for pieces rotating at the top of the board.
    Piece* currentPiece;
    Piece* nextPiece;
    PieceFactory* pieceFactory;
    Score score;
    Position textPos;

    void ClearLines();
    void ClearLine(int);
    void SetPieceValues(int);

    bool Transition();
    
    bool CheckDAS();
    bool CheckRotationDAS();
    void ResetDAS();
    void ResetRotationDAS();

    bool PieceCanFall();

    int GetFallSpeed(int);

    void PrintNextPiece();
};
#endif