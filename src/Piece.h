#ifndef PIECE_H
#define PIECE_H

#include<Windows.h>
#include"Position.h"

using namespace std;

class Piece
{
    protected:
    Position* pivot;//the pivot piece
    Position** cells;//array of positions of the other pieces

    public:
    const int NUM_CELLS;

    Piece(Position*, int);
    ~Piece();

    void MoveLeft();//moves piece left
    void MoveRight();//moves piece right

    virtual void RRotation();//right rotation
    virtual void LRotation();//left rotation

    void Fall();//moves piece down

    Position** GetPositions();

    private:
    virtual void SetCells() = 0;

    void Move(int,int);
    void Rotate(int[2][2]);
};

class TPiece: public Piece
{
    public:
    TPiece(Position* = new Position(0,0));

    private:
    void SetCells() override;
};

class OPiece: public Piece
{
    public:
    OPiece(Position* = new Position(0,0));

    private:
    void SetCells() override;

    void RRotation() override;
    void LRotation() override;
};

class IPiece: public Piece
{
    public:
    IPiece(Position* = new Position(0,0));

    private:
    bool up;
    void SetCells() override;

    void RRotation() override;
    void LRotation() override;
};

class LJPiece: public Piece
{
    public:
    LJPiece(bool, Position* = new Position(0,0));

    private:
    bool isL;
    void SetCells() override;
};

class SZPiece: public Piece
{
    public:
    SZPiece(bool, Position* = new Position(0,0));

    private:
    bool isS;
    void SetCells() override;
};
#endif