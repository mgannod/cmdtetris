#ifndef SCORE_H
#define SCORE_H

using namespace std;

class Score
{
    public:
    Score();

    void LinesCleared(int,int);
    int GetScore();

    private:
    int score;
};
#endif