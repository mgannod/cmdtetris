#ifndef TETRIS_H
#define TETRIS_H

#include<iostream>
#include<fstream>
#include<sstream>
#include<vector>
#include<algorithm>
#include<thread>
#include<chrono>

#include "Board.h"
#include "Piece.h"
#include "..\libs\PDCurses-master\curses.h"

using namespace std;

struct highScore
{
    string name;
    int score;
};

Board* board;
vector<highScore> highScores;

void DrawFrame(int);
void UpdateScreen();
int Tetris(int = 5);
void Menu();
void PrintMenu();
int SelectSpeed();
vector<highScore> GetScores();
void SaveScores();
bool IsHighScore(int);
void AddHighScore(string, int);

#endif