#include "Tetris.h"

int main()
{
    initscr();
    noecho();
    highScores = GetScores();
    Menu();
    
    endwin();
    delete board;
    return 0;
}

void Menu()
{
    bool flag = true;
    PrintMenu();
    int cursorPos = 7;
    while(flag)
    {
        int ch = getch();//get input
        //for 'w'=119 and 's'=115 adjust cursor position
        switch(ch)
        {
            case 119:
                mvprintw(cursorPos, 0, " ");

                if(cursorPos == 7)//wrap to bottom if at the top
                    cursorPos = 9;
                else
                    cursorPos--;

                mvprintw(cursorPos, 0, ">");
                break;
            case 115:
                mvprintw(cursorPos, 0, " ");

                if(cursorPos == 9)//wrap to top if at the bottom
                    cursorPos = 7;
                else
                    cursorPos++;

                mvprintw(cursorPos, 0, ">");
                break;
            case 10:
                if(cursorPos == 7)//Play
                {
                    int score = Tetris(SelectSpeed());//Get start speed
                    if(IsHighScore(score))
                    {
                        flushinp();
                        mvprintw(0, 0, "Highscore! Enter name: ");
                        char str[80];
                        echo();
                        getstr(str);
                        noecho();
                        move(0, 0);
                        clrtoeol();
                        AddHighScore(str, score);
                        SaveScores();
                    }
                    flushinp();
                    
                    mvprintw(0, 0, "Press enter to continue");
                    while(getch() != 10);//wait until enter is pressed board to display
                    
                    PrintMenu();
                    flushinp();
                    cursorPos = 7;
                }
                else if(cursorPos == 8)//Options
                {
                    //Options menu
                }
                else if(cursorPos == 9)//Quit
                    flag = false;
                break;
        }
    }
}

void PrintMenu()
{
    /*
        scores
        1.12345
        2.2345
        3.345
        4.45
        5.5

        > Play
          Options
          Quit
    */
    erase();
    mvprintw(0, 0, "Scores:\n1. %s %dpts\n2. %s %dpts\n3. %s %dpts\n4. %s %dpts\n5. %s %dpts", 
                    highScores.at(0).name.c_str(), highScores.at(0).score, 
                    highScores.at(1).name.c_str(), highScores.at(1).score, 
                    highScores.at(2).name.c_str(), highScores.at(2).score, 
                    highScores.at(3).name.c_str(), highScores.at(3).score, 
                    highScores.at(4).name.c_str(), highScores.at(4).score);
    mvprintw(7, 0, "> Play");
    mvprintw(8, 2, "Options");
    mvprintw(9, 2, "Quit");
    move(7, 1);
    refresh();
}

int SelectSpeed()
{
    erase();
    int y = 0, x = 0, level = 0;
    mvprintw(0, 0, ">  0   1   2   3   4\n   5   6   7   8   9\n  10  11  12  13  14\n  15  16  17  18  19");
    move(0, 1);
    refresh();
    bool selected = false;

    while(!selected)
    {
        int ch = getch();//get input

        //for 'a'=97 'd'=100 'w'=119 and 's'=115 adjust cursor position
        switch(ch)
        {
            case 97:
                mvprintw(y, 4*x, " ");
                if(x > 0)
                    x--;
                mvprintw(y, 4*x, ">");
                break;
            case 100:
                mvprintw(y, 4*x, " ");
                if(x < 4)
                    x++;
                mvprintw(y, 4*x, ">");
                break;
            case 119:
                mvprintw(y, 4*x, " ");
                if(y > 0)
                    y--;
                mvprintw(y, 4*x, ">");
                break;
            case 115:
                mvprintw(y, 4*x, " ");
                if(y < 3)
                    y++;
                mvprintw(y, 4*x, ">");
                break;
            case 10:
                selected = true;
                level = x + 5*y;
                break;
        }
    }
    return level;
}

int Tetris(int startLevel)
{
    board = new Board(startLevel);

    static auto lastLoopTime = chrono::high_resolution_clock::now();
    chrono::nanoseconds lastFPSTime = chrono::seconds(0);
    const int TARGET_FPS = 60;
    const chrono::nanoseconds OPTIMAL_TIME = chrono::nanoseconds(1000000000)/TARGET_FPS;
    static int frames = 0;
    static int fps = 0;
//*    
    bool flag = true;
    erase();
    move(1,0);
    board->PrintBoard();
    while(!board->GameOver() && flag)
    {
        auto now = chrono::high_resolution_clock::now();
        auto updateLength = now - lastLoopTime;
        lastLoopTime = now;

        lastFPSTime += updateLength;
        frames++;
        
        //once a second passes, update frame info
        if(lastFPSTime >= chrono::seconds(1))
        {
            fps = frames;
            lastFPSTime = chrono::seconds(0);
            frames = 0;
        }

        //update screen and board
        DrawFrame(fps);
        UpdateScreen();

        if(GetKeyState('P') & 0x8000)//Check if high-order bit is set (1 << 15)
        {
            flag = false;//break from loop
        }
        
        //pause if time left in frame
        chrono::nanoseconds sleepTime = OPTIMAL_TIME - (chrono::high_resolution_clock::now() - lastLoopTime);
        this_thread::sleep_for(sleepTime);
    }//*/
    int score = board->GetScore();
    
    return score;
}

//draws the frame
void DrawFrame(int fps)
{
    move(0,0);
    printw("FPS: %d", fps);//update fps
    board->Update();//update board
}

//updates the frame and prints it
void UpdateScreen()
{
    move(0,0);
    refresh();
}

vector<highScore> GetScores()
{
    //open file
    ifstream fin("scores.csv");

    if(!fin.is_open()) throw runtime_error("Could not open file scores.csv");

    vector<highScore> row;
    string line;
    string name;
    string val;

    //break into pieces by ','
    while(getline(fin, line))
    { 
        stringstream ss(line);
        getline(ss, name, ',');
        getline(ss, val, ',');
        row.push_back({name, stoi(val)});
    }

    fin.close();

    return row;
}

void SaveScores()
{
    ofstream fout("scores.csv");

    for(int i = 0; i < 5; i++)
    {
        fout << highScores.at(i).name << ',' << highScores.at(i).score << endl;
    }

    fout.close();
}

bool IsHighScore(int score)
{
    bool isHS = false;
    if(score > highScores.at(4).score)
        isHS = true;
    return isHS;
}

bool CompareScores(highScore a, highScore b){return a.score > b.score;}
void AddHighScore(string name, int score)
{
    highScores.push_back({name, score});//add in high score
    sort(highScores.begin(), highScores.end(), CompareScores);//sort it greatest to smallest
    highScores.pop_back();//remove the smallest
}