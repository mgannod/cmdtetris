#ifndef PIECEFACTORY_H
#define PIECEFACTORY_H

#include "Piece.h"

using namespace std;

class PieceFactory
{
    public:
    PieceFactory();
    Piece* GetNextPiece();
};

#endif