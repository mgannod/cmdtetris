#include "Piece.h"
#include "Position.h"

using namespace std;

Piece::Piece(Position* position, int numCells): NUM_CELLS(numCells)
{
    this->pivot = position;
    cells = new Position*[numCells];
    cells[0] = pivot;//set cell 0 to pivot
}

Piece::~Piece()
{
    delete[] cells;
    delete cells;
    delete pivot;
}

Position** Piece::GetPositions()
{
    return cells;
}

void Piece::Fall()
{
    Move(0, -1);
}

void Piece::MoveLeft()
{
    //Set new positions
    Move(-1,0);
}

void Piece::MoveRight()
{
    //Set new positions
    Move(1,0);
}

//translates a piece on x and y
void Piece::Move(int x, int y)
{
    //set cells
    for(int i = 0; i < NUM_CELLS; i++)
    {
        Position cellPos = *cells[i];
        cells[i]->SetPosition(cellPos.GetX()+x, cellPos.GetY()+y);
    }
}

void Piece::Rotate(int rotationMatrix[2][2])
{
    Position* pos = new Position[NUM_CELLS];
    bool canRotate = true;
    //set new positions
    for(int i = 1; i < NUM_CELLS && canRotate; i++)
    {
        //get vector to cell
        Position c = *cells[i];
        int v[2] = {c.GetX() - pivot->GetX(), c.GetY() - pivot->GetY()};

        //multiply matrices
        int changeVector[2];
        for(int col = 0; col < 2; col++)
        {
            changeVector[col] = rotationMatrix[0][col] * v[0] + rotationMatrix[1][col] * v[1];
        }

        //get new cell positions x y
        int x = pivot->GetX()+changeVector[0];
        int y = pivot->GetY()+changeVector[1];
        pos[i] = Position(x,y);
    }

    //set rotated positions
    for(int i = 1; i < NUM_CELLS; i++)
    {
        cells[i]->SetPosition(pos[i].GetX(), pos[i].GetY());
    }

    delete[] pos;
}

void Piece::LRotation()
{
    int leftMatrix[2][2] = { {0,1}, {-1,0} };//rotation matrix
    
    Rotate(leftMatrix);
}

void Piece::RRotation()
{
    int rightMatrix[2][2] = { {0,-1}, {1,0} };//rotation matrix
    
    Rotate(rightMatrix);
}