#ifndef BOARDCELL_H
#define BOARDCELL_H

#include<string>
#include"Position.h"

using namespace std;

class BoardCell
{
    private:
    Position pos;
    int cellValue;//value of the cell

    public:
    //get set
    int GetValue();
    void SetValue(int);

    //CONSTRUCTOR
    BoardCell(Position = Position(0,0));

    //Returns a position
    Position GetPosition();
};
#endif