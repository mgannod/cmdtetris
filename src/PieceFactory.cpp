#include "PieceFactory.h"
#include <stdlib.h>
#include <time.h>

using namespace std;

PieceFactory::PieceFactory()
{
    srand(time(NULL));//seed random
}

Piece* PieceFactory::GetNextPiece()
{
    int i = rand() % 7;
    Piece* nextPiece;
    switch(i)
    {
        case 0:
            nextPiece = new IPiece(new Position(4, 19));
            break;
        case 1:
            nextPiece = new SZPiece(true, new Position(4, 18));
            break;
        case 2:
            nextPiece = new SZPiece(false, new Position(4, 18));
            break;
        case 3:
            nextPiece = new LJPiece(true, new Position(4, 18));
            break;
        case 4:
            nextPiece = new LJPiece(false, new Position(4, 18));
            break;
        case 5:
            nextPiece = new OPiece(new Position(4, 18));
            break;
        case 6:
            nextPiece = new TPiece(new Position(4, 18));
            break;
    }

    return nextPiece;
}